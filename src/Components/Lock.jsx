import { Component } from "react";

export default class Lock extends Component {
  render() {
    return (
      <button
        onClick={this.props.action}
        style={{
          width: "100px",
          height: "30px",
          padding: "5px",
          fontWeight: "bold",
          outline: "none",
          cursor: "pointer",
          marginTop: "10px",
        }}
      >
        {!this.props.locked ? "Travar" : "Destravar"}
      </button>
    );
  }
}
