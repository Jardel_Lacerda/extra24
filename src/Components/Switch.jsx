import { Component } from "react";

export default class Switch extends Component {
  render() {
    return (
      <button
        onClick={this.props.action}
        disabled={this.props.locked}
        style={{
          width: "100px",
          height: "30px",
          padding: "5px",
          fontWeight: "bold",
          outline: "none",
          cursor: "pointer",
        }}
      >
        {this.props.showLogo ? "Desligar" : "Ligar"}
      </button>
    );
  }
}
